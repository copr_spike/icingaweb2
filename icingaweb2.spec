# Icinga Web 2 | (c) 2013-2017 Icinga Development Team | GPLv2+

%global commit f905a978d8b3b968010fdee99521bda887b2d8c0
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           icingaweb2
Version:        2.5.3
Release:        2.20180610git%{shortcommit}%{?dist}
Summary:        Icinga Web 2
License:        GPLv2+ and MIT and BSD
URL:            https://icinga.com
Source0:        https://github.com/Icinga/%{name}/archive/%{commit}/%{name}-%{shortcommit}.tar.gz
BuildArch:      noarch

%define wwwconfigdir    %{_sysconfdir}/httpd/conf.d
%define wwwuser         apache
%define icingawebgroup  icingaweb2
%define configdir       %{_sysconfdir}/%{name}
%define logdir          %{_localstatedir}/log/%{name}
%define docsdir         %{_datadir}/doc/%{name}

Requires:               php php-common php-mysqlnd php-pgsql php-ldap
Requires:               icingacli = %{version}-%{release}
Requires:               %{name}-common = %{version}-%{release}
Requires:               php-Icinga = %{version}-%{release}
Requires:               %{name}-vendor-dompdf = %{version}-%{release}
Requires:               %{name}-vendor-HTMLPurifier = 1:%{version}-%{release}
Requires:               %{name}-vendor-JShrink = %{version}-%{release}
Requires:               %{name}-vendor-lessphp = %{version}-%{release}
Requires:               %{name}-vendor-Parsedown = %{version}-%{release}
Requires(pre):          shadow-utils

%description
Icinga Web 2


%package common
Summary:                Common files for Icinga Web 2 and the Icinga CLI
Requires(pre):          shadow-utils

%description common
Common files for Icinga Web 2 and the Icinga CLI


%package -n php-Icinga
Summary:                    Icinga Web 2 PHP library
Requires:                   php-common
Requires:                   php-gd php-intl php-mbstring php-pecl-imagick
Requires:                   %{name}-vendor-zf1 = %{version}-%{release}

%description -n php-Icinga
Icinga Web 2 PHP library


%package -n icingacli
Summary:                    Icinga CLI
Requires:                   %{name}-common = %{version}-%{release}
Requires:                   php-Icinga = %{version}-%{release}
Requires:                   bash-completion
Requires:                   php-cli

%description -n icingacli
Icinga CLI


%define selinux_variants mls targeted

%package selinux
Summary:        SELinux policy for Icinga Web 2
BuildRequires:  checkpolicy, selinux-policy-devel, hardlink
Requires:           %{name} = %{version}-%{release}
Requires(post):     policycoreutils
Requires(postun):   policycoreutils

%description selinux
SELinux policy for Icinga Web 2


%package vendor-dompdf
Summary:    Icinga Web 2 vendor library dompdf
License:    LGPLv2.1
Requires:   php-common
Requires:   %{name}-common = %{version}-%{release}

%description vendor-dompdf
Icinga Web 2 vendor library dompdf


%package vendor-HTMLPurifier
Epoch:      1
Summary:    Icinga Web 2 vendor library HTMLPurifier
License:    LGPLv2.1
Requires:   php-common
Requires:   %{name}-common = %{version}-%{release}

%description vendor-HTMLPurifier
Icinga Web 2 vendor library HTMLPurifier


%package vendor-JShrink
Summary:    Icinga Web 2 vendor library JShrink
License:    BSD
Requires:   php-common
Requires:   %{name}-common = %{version}-%{release}

%description vendor-JShrink
Icinga Web 2 vendor library JShrink


%package vendor-lessphp
Summary:    Icinga Web 2 vendor library lessphp
License:    MIT
Requires:   php-common
Requires:   %{name}-common = %{version}-%{release}

%description vendor-lessphp
Icinga Web 2 vendor library lessphp


%package vendor-Parsedown
Summary:    Icinga Web 2 vendor library Parsedown
License:    MIT
Requires:   php-common
Requires:   %{name}-common = %{version}-%{release}

%description vendor-Parsedown
Icinga Web 2 vendor library Parsedown


%package vendor-zf1
Summary:    Icinga Web 2's fork of Zend Framework 1
License:    BSD
Requires:   php-common
Obsoletes:  %{name}-vendor-Zend < 1.12.20
Requires:   %{name}-common = %{version}-%{release}

%description vendor-zf1
Icinga Web 2's fork of Zend Framework 1


%prep
%autosetup -n %{name}-%{commit}
mkdir selinux
cp -p packages/selinux/icingaweb2.{fc,if,te} selinux

%build
cd selinux
for selinuxvariant in %{selinux_variants}
do
  make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile
  mv icingaweb2.pp icingaweb2.pp.${selinuxvariant}
  make NAME=${selinuxvariant} -f /usr/share/selinux/devel/Makefile clean
done
cd -

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/{%{_datadir}/%{name}/{modules,library/vendor,public},%{_bindir},%{configdir}/modules,%{logdir},%{_datadir}/php,%{wwwconfigdir},%{_sysconfdir}/bash_completion.d,%{docsdir}}
cp -prv application doc %{buildroot}/%{_datadir}/%{name}
cp -pv etc/bash_completion.d/icingacli %{buildroot}/%{_sysconfdir}/bash_completion.d/icingacli
cp -prv modules/{monitoring,setup,doc,translation} %{buildroot}/%{_datadir}/%{name}/modules
cp -prv library/Icinga %{buildroot}/%{_datadir}/php
cp -prv library/vendor/{dompdf,HTMLPurifier*,JShrink,lessphp,Parsedown,Zend} %{buildroot}/%{_datadir}/%{name}/library/vendor
cp -prv public/{css,font,img,js,error_norewrite.html,error_unavailable.html} %{buildroot}/%{_datadir}/%{name}/public
cp -pv packages/files/apache/icingaweb2.fpm.conf %{buildroot}/%{wwwconfigdir}/icingaweb2.conf
cp -pv packages/files/bin/icingacli %{buildroot}/%{_bindir}
cp -pv packages/files/public/index.php %{buildroot}/%{_datadir}/%{name}/public
cp -prv etc/schema %{buildroot}/%{docsdir}
cp -prv packages/files/config/modules/{setup,translation} %{buildroot}/%{configdir}/modules
cd selinux
for selinuxvariant in %{selinux_variants}
do
  install -d %{buildroot}%{_datadir}/selinux/${selinuxvariant}
  install -p -m 644 icingaweb2.pp.${selinuxvariant} %{buildroot}%{_datadir}/selinux/${selinuxvariant}/icingaweb2.pp
done
cd -
# TODO: Fix build problems on Icinga, see https://github.com/Icinga/puppet-icinga_build/issues/11
#/usr/sbin/hardlink -cv %{buildroot}%{_datadir}/selinux

%pre
getent group icingacmd >/dev/null || groupadd -r icingacmd
usermod -a -G icingacmd,%{icingawebgroup} %{wwwuser}
exit 0

%files
%{_datadir}/%{name}/application/controllers
%{_datadir}/%{name}/application/fonts
%{_datadir}/%{name}/application/forms
%{_datadir}/%{name}/application/layouts
%{_datadir}/%{name}/application/views
%{_datadir}/%{name}/application/VERSION
%{_datadir}/%{name}/doc
%{_datadir}/%{name}/modules
%{_datadir}/%{name}/public
%config(noreplace) %{wwwconfigdir}/icingaweb2.conf
%attr(2775,root,%{icingawebgroup}) %dir %{logdir}
%attr(2770,root,%{icingawebgroup}) %config(noreplace) %dir %{configdir}/modules/setup
%attr(0660,root,%{icingawebgroup}) %config(noreplace) %{configdir}/modules/setup/config.ini
%attr(2770,root,%{icingawebgroup}) %config(noreplace) %dir %{configdir}/modules/translation
%attr(0660,root,%{icingawebgroup}) %config(noreplace) %{configdir}/modules/translation/config.ini
%{docsdir}
%docdir %{docsdir}


%pre common
getent group %{icingawebgroup} >/dev/null || groupadd -r %{icingawebgroup}
exit 0

%files common
%dir %{_datadir}/%{name}
%dir %{_datadir}/%{name}/application
%dir %{_datadir}/%{name}/library
%dir %{_datadir}/%{name}/library/vendor
%dir %{_datadir}/%{name}/modules
%{_datadir}/%{name}/application/locale
%attr(2770,root,%{icingawebgroup}) %config(noreplace) %dir %{configdir}
%attr(2770,root,%{icingawebgroup}) %config(noreplace) %dir %{configdir}/modules

%files -n php-Icinga
%defattr(-,root,root)
%{_datadir}/php/Icinga

%files -n icingacli
%{_datadir}/%{name}/application/clicommands
%{_sysconfdir}/bash_completion.d/icingacli
%attr(0755,root,root) %{_bindir}/icingacli


%post selinux
for selinuxvariant in %{selinux_variants}
do
  %{_sbindir}/semodule -s ${selinuxvariant} -i %{_datadir}/selinux/${selinuxvariant}/icingaweb2.pp &> /dev/null || :
done
%{_sbindir}/restorecon -R %{_datadir}/%{name} &> /dev/null || :
%{_sbindir}/restorecon -R %{configdir} &> /dev/null || :
%{_sbindir}/restorecon -R %{logdir} &> /dev/null || :

%postun selinux
if [ $1 -eq 0 ] ; then
  for selinuxvariant in %{selinux_variants}
  do
     %{_sbindir}/semodule -s ${selinuxvariant} -r icingaweb2 &> /dev/null || :
  done
  [ -d %{_datadir}/%{name} ] && %{_sbindir}/restorecon -R %{_datadir}/%{name} &> /dev/null || :
  [ -d %{configdir} ] && %{_sbindir}/restorecon -R %{configdir} &> /dev/null || :
  [ -d %{logdir} ] && %{_sbindir}/restorecon -R %{logdir} &> /dev/null || :
fi

%files selinux
%doc selinux/*
%{_datadir}/selinux/*/icingaweb2.pp

%files vendor-dompdf
%{_datadir}/%{name}/library/vendor/dompdf

%files vendor-HTMLPurifier
%{_datadir}/%{name}/library/vendor/HTMLPurifier
%{_datadir}/%{name}/library/vendor/HTMLPurifier.autoload.php
%{_datadir}/%{name}/library/vendor/HTMLPurifier.php

%files vendor-JShrink
%{_datadir}/%{name}/library/vendor/JShrink

%files vendor-lessphp
%{_datadir}/%{name}/library/vendor/lessphp

%files vendor-Parsedown
%{_datadir}/%{name}/library/vendor/Parsedown

%files vendor-zf1
%{_datadir}/%{name}/library/vendor/Zend

%changelog
* Sun Jun 10 2018 spike <spike@fedoraproject.org> 2.5.3-2.20180610gitf905a978
- Updated to latest git commit

* Fri Apr 27 2018 Eric Lippmann <eric.lippmann@icinga.com> 2.5.3-1
- Update to 2.5.3

* Thu Apr 26 2018 Eric Lippmann <eric.lippmann@icinga.com> 2.5.2-1
- Update to 2.5.2

* Mon Jan 22 2018 Markus Frosch <markus.frosch@icinga.com> 2.5.1-1
- Update to 2.5.1
- Remove FPM patches

* Wed Nov 29 2017 Eric Lippmann <eric.lippmann@icinga.com> 2.5.0-2
- FPM: Add patch to support both Apache >= 2.4 and Apache < 2.4

* Tue Nov 28 2017 Eric Lippmann <eric.lippmann@icinga.com> 2.5.0-1
- Install error_unavailable.html
- Add patch to fix Apache FPM config

* Mon Nov 27 2017 Markus Frosch <markus.frosch@icinga.com> 2.5.0-1
- Update to 2.5.0
- All packages now require PHP >= 5.6
- [EPEL 6 + 7] We now require PHP 7 from SCL packages, php-fpm as runtime engine
- [SUSE / openSUSE] Requirements will force the installation of php7
- Please check upgrading docs at /usr/share/icingaweb2/doc/80-Upgrading.md

* Thu Sep 28 2017 Markus Frosch <markus.frosch@icinga.com> 2.4.2-1
- Update to 2.4.2
